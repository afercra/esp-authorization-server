package gov.mil.esp.authorizationserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@EnableDiscoveryClient
@SpringBootApplication
public class EspAuthorizationServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(EspAuthorizationServerApplication.class, args);
	}

}
